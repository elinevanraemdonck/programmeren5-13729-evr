/**
 * Load Drive API client library.
 * Samples: https://advancedweb.hu/2015/05/26/accessing-google-drive-in-javascript/
 */
function makeDriveApiCall() {
    gapi.client.load('drive', 'v3', isUploadFolderPresent);
}

/**
 * List files.
 */
function listRawFiles() {
    var request = gapi.client.drive.files.list({
        'pageSize': 10,
        'fields': "nextPageToken, files(id, name)"
    });

    request.execute(function(resp) {
        var files = resp.files;
        if (files && files.length > 0) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                appendListItem(file.name + ' (' + file.id + ')');
            }
        }
        else {
            appendListItem('No files found.');
            showFeedback(JSON.stringify(resp, 4));
        }
    });
}

/**
 * Append a li element to the list in the explorer 
 * with the given text as its text node.
 *
 * @param {string} text Text to be placed in the li element.
 */
function appendListItem(text) {
    var list= document.querySelector('#explorer ul');
    if (!list) {
        list = document.createElement('ul');
        var explorer = document.getElementById('explorer');
        explorer.appendChild(list);
    }
    var listItem = document.createElement('li');
    var textContent = document.createTextNode(text);
    listItem.appendChild(textContent);
    list.appendChild(listItem);
}

/**
 * Clear files and folders in the Explorer
 */
function clearExplorer () {
    var explorer = document.getElementById('explorer');
    explorer.innerHTML = '';
   
}

/**
 * Append a p element to the feedback 
 * with the given text as its text node.
 *
 * @param {string} text Text to be placed in the p element.
 */
function showFeedback(text) {
    var feedback = document.getElementById('feedback');
    if (!feedback) {
        feedback = document.createElement('div');
        document.body.appendChild(feedback);
    }
    var p = document.createElement('p');
    var textContent = document.createTextNode(text);
    p.appendChild(textContent);
    feedback.appendChild(p);
}

/** 
* This returns a promise with the result. 
* One thing we should do is to first check if the folder is present, 
* and only create it if it is not. 
* This is a simple listing with filters to the directory mime type 
* and not trashed (m.a.w. gedeleted door de gebruiker), 
* then check if there is a result.
*/
function isUploadFolderPresent(){
    return gapi.client.drive.files.list(
        {q:"mimeType = 'application/vnd.google-apps.folder' and trashed = false"}
    ).then(function(files){
            showFeedback(JSON.stringify(files, 4));
        var directory=files.result.files;

        if(!directory.length){
            return createFolder('myap').then(function(res){
                return res.result;
            });
        }else{
            return directory[0];
        }
    });
}

function createFolder(title){
    return gapi.client.drive.files.insert(
        {
            'resource':{
                "title": title,
                "mimeType": "application/vnd.google-apps.folder"
            }
        }
    )
}