function getFiles() {
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token; // or this: gapi.auth.getToken().access_token;
    var ajax = new XMLHttpRequest();
    ajax.open("GET", "https://www.googleapis.com/drive/v3/files?q=not+trashed&fields=files(iconLink%2Cname)", true);
    ajax.setRequestHeader('Authorization', 'Bearer ' + accessToken);
    // ajax.responseType = 'arraybuffer'
    ajax.onreadystatechange = function() {
        // server sent response and 
        // OK 200 The request was fulfilled. 
        if (ajax.readyState == 4 && ajax.status == 200) {
           var response = JSON.parse(ajax.responseText);
        var ul = document.getElementById("list");
        for (var i = 0; i < response.files.length; i++) {
            var item = response.files[i];
            var li = document.createElement("li");
            li.innerHTML = "<img src='" + item.iconLink + "'> " + item.name;
            ul.appendChild(li);
        }

        }
    };
    ajax.send();
}

function getFolders() {
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token; // or this: gapi.auth.getToken().access_token;
    var ajax = new XMLHttpRequest();
    ajax.open("GET", "https://www.googleapis.com/drive/v3/files?q=not+trashed+and+mimeType='application/vnd.google-apps.folder'&fields=files(iconLink%2Cname)", true);
    ajax.setRequestHeader('Authorization', 'Bearer ' + accessToken);
    // ajax.responseType = 'arraybuffer'
    ajax.onreadystatechange = function() {
        // server sent response and 
        // OK 200 The request was fulfilled. 
        if (ajax.readyState == 4 && ajax.status == 200) {
           var response = JSON.parse(ajax.responseText);
        var ul = document.getElementById("list");
        for (var i = 0; i < response.files.length; i++) {
            var item = response.files[i];
            var li = document.createElement("li");
            li.innerHTML = "<img src='" + item.iconLink + "'> " + item.name;
            ul.appendChild(li);
        }

        }
    };
    ajax.send();
}

function getFilesInFolders(id) {
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token; // or this: gapi.auth.getToken().access_token;
    var ajax = new XMLHttpRequest();
    ajax.open("GET", "https://www.googleapis.com/drive/v3/files?q=not+trashed+and+mimeType='application/vnd.google-apps.folder'&fields=files(iconLink%2Cname)", true);
    ajax.setRequestHeader('Authorization', 'Bearer ' + accessToken);
    // ajax.responseType = 'arraybuffer'
    ajax.onreadystatechange = function() {
        // server sent response and 
        // OK 200 The request was fulfilled. 
        if (ajax.readyState == 4 && ajax.status == 200) {
           var response = JSON.parse(ajax.responseText);
        var ul = document.getElementById("list");
        for (var i = 0; i < response.files.length; i++) {
            var item = response.files[i];
            var li = document.createElement("li");
            li.innerHTML = "<img src='" + item.iconLink + "'> " + item.name;
            ul.appendChild(li);
        }

        }
    };
    ajax.send();
}
