//constructor sorteerbare tabel
function SorteerbareTabel(id) {
    //referentie naar tabelelement wordt opgehaald via DOM-methode
    // ref wordt opgeslagen in variable this.tabel
    this.tbl = document.getElementById(id);
    this.lastSortedTh = null;
    
    //check of een element met dat id bestaat en of die wel degelijk naar een tabel verwijst
    if (typeof(this.tbl) != 'undefined' && this.tbl.nodeName == "TABLE") {
        var tabelHoofden = this.tbl.tHead.rows[0].cells;
        // hier wordt bepaald welke kolom het laatst gesorteerd werd (diegene met asc/dsc in classname)
        for (var i = 0; i < tabelHoofden; i++) {
            if (tabelHoofden[i].className.match(/asc|dsc/)) {
                this.lastSortedTh = tabelHoofden[i];
            }
        }
        //deze functie maakt de gecreëerde tabel sorteerbaar
        this.maakSorteerbaar();
    }
}

// moet prototypefunctie zijn, anders werkt het niet
SorteerbareTabel.prototype.maakSorteerbaar = function () {
    // tabelhoofden = de cellen van de eerste rij in de tabel
    var tabelHoofden = this.tbl.tHead.rows[0].cells;
    for (var i = 0; i < tabelHoofden.length; i++) {
	    tabelHoofden[i].cIdx = i;
	 // a tags rond <th> elementen plaatsen
        var a = document.createElement("a");
            a.href = "#";
            //inhoud van <th> wordt in <a> geplaatst
            a.innerHTML = tabelHoofden[i].innerHTML;
            // this zou verwijzen naar a, daarom that
            //that verwijst naar instantie van SorteerbareTabel
            a.addEventListener('click',function (that) {
                return function () {
                    // this verwijst naar a
                    that.sortCol(this);
                    // zorgt ervoor dat href value niet gevolgd wordt:
                    return false;
                }
            }(this),false);
        
        tabelHoofden[i].innerHTML = "";
        tabelHoofden[i].appendChild(a);
    }
}

// el verwijst naar anchor waarop geklikt werd
SorteerbareTabel.prototype.sortCol = function (el) {

     // rijen binnen de tabel
    var rows = this.tbl.rows;
    // arrays om alfanumerieke en numerieke inhoud van de cellen in de kolom op te slaan
    var alpha = [], numeric = [];
    // respectievelijke indexen
    var aIdx = 0, nIdx = 0;
    // th is parent van de anchor waarop geklikt werd (het kolomhoofd)
    var th = el.parentNode;
    var cellIndex = th.cIdx;
    for (var i = 1; rows[i]; i++) {
        var cell = rows[i].cells[cellIndex];
        //Firefox ondersteunt enkel textContent, IE ondersteunt enkel innerText
        var content = cell.textContent ? cell.textContent : cell.innerText;
      /* inhoud van cellen wordt in een van de twee arrays ondergebracht, naargelang het type
        ook een verwijzing naar de parentrow wordt opgeslagen
        deze zal gebruikt worden om de html tabelrijen te herschikken
        zodra de nieuwe volgorde bepaald is
         */
        var num = content.replace(/(\$|\,|\s)/g, "");
        //parsefloat converteert tekst naar float datatype
          if (parseFloat(num) == num) { 
            numeric[nIdx++] = {
                value: Number(num),
                row: rows[i]
            }
        } else {
            alpha[aIdx++] = {
                value: content,
                row: rows[i]
            }
        }
    }
    
    // hier wordt oplopend of aflopend gesorteerd
    var col = [], top, bottom;
    if (th.className.match("asc")) {
        //aflopend sorteren (want is momenteel oplopend gesorteerd --> className ="asc")
        //bij aflopend komen de alfabetische waarden eerst, dus top = alpha
        top = sorteer(alpha, -1);
        bottom = sorteer(numeric, -1);
        //className aanpassen zodat die huidige sorteerwijze reflecteert
        th.className = th.className.replace(/asc/, "dsc");
    } else {
        //oplopend sorteren 
        top = sorteer(numeric, 1);
        bottom = sorteer(alpha, 1);
        if (th.className.match("dsc")) {
            th.className = th.className.replace(/dsc/, "asc");
        } else {
            th.className += "asc";
        }
    }
    
    
    //hier wordt de laatstgesorteerde kolomheader ontdaan van de "asc/dsc" in de classname
    // behalve indien de laatstgesorteerde kolomheader gelijk is aan de huidige
    //nadien wordt de huidige kolomheader de lastSortedTh

    if (this.lastSortedTh && th != this.lastSortedTh) {
        this.lastSortedTh.className = this.lastSortedTh.className.replace(/dsc|asc/g, "");
    }
    this.lastSortedTh = th;
    
    //de "top"- en "bottom"- array worden samengplakt
    col = top.concat(bottom);
    // de eerste rij van de tabel is tBodies[0] = de kolomheaders
    var tBody = this.tbl.tBodies[0];
    // de volgende rijen worden eronder geplakt gerangschikt volgens de volgorde in de "col"-array
    for (var i = 0; col[i]; i++) {
        tBody.appendChild(col[i].row);
    }
}


function sorteer (arr, dir){
    //oplopend
    if (dir ===1){
        arr.sort(function(a , b){return a - b});
        }
    //aflopend
    else if (dir === -1){
        arr.sort(function(a , b){return a + b});
        }
    return arr;
    }
    
    
    

   