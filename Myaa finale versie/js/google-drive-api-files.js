var currentFileId = null;
var currentFileName;


/**
 * Drive API Helper function prepareUploadText
 * Get text out from editor
 * Validate file name and call uploadText method if file name is valid
 * 
 */
function prepareUploadText() {
    // get the text from the editor
    var text = document.getElementById('editor').value;
    if (text.length > 0) {
        // get the file name
        // Eerst gedacht met parser to doen, maar dit is te streng
        // https://developer.mozilla.org/en-US/docs/Web/API/DOMParser
        // var parser = new DOMParser();
        // var doc = parser.parseFromString(text, "application/xml");
        // if (doc.getElementsByTagName('h1').length == 0) {
        var name = text.match(/<h1>(.*?)<\/h1>/g);
        if (!name) {
            alert('Je moet een h1 element toevoegen waarin de naam van het bestand staat!');
        }
        else {
            // we need the first match
            var fileName = name[0];
            // remove tags, dat kan waarschijnlijk eleganter...
            // maar ik heb hier al genoeg over moeten nadenken
            fileName = fileName.replace('<h1>', '');
            fileName = fileName.replace('</h1>', '');
            fileName = fileName.replace(/[^a-zA-Z0-9_. ]+/, '');
            // haal de Id van de parent folder op
            var parentFolderId = document.getElementById('currentFolderId').value;
            uploadText(fileName, text, parentFolderId);
        }
    }
    else {
        alert('Editor is leeg, kan geen leeg bestand uploaden...');
    }
}

function prepareSaveText() {
    // get the text from the editor
    var text = document.getElementById('editor').value;
    if (text.length > 0) {
            // haal de Id van de parent folder op
            var parentFolderId = document.getElementById('currentFolderId').value;
            saveText(text,currentFileId);
    }
    
    else {
        alert('Editor is leeg, kan geen leeg bestand uploaden...');
    }
}

/**
 * Drive API Helper function uploadText
 * https://developers.google.com/drive/v3/web/manage-uploads#multipart
 * Simple upload: uploadType=media. 
 * For quick transfer of smaller files, for example, 5 MB or less.
 * 
 * @param name {string} the file name
 * @param text {string} the html content of the file
 * @param parentFolderId {string} the id of the parent folder of file
 */
function uploadText(name, text, parentFolderId) {

    var metaData = {
        'name': name + '.html',
        'parents': [parentFolderId]
    };

    var data = '--next_section\r\n' +
        'Content-Type: application/json; charset=UTF-8\r\n\r\n' +
        JSON.stringify(metaData) +
        '\r\n\r\n--next_section\r\n' +
        'Content-Type: application/text\r\n\r\n' +
        text +
        '\r\n--next_section--';

    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    ajax.postRequest('https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart',
        data,
        function(responseText) {
            alert(responseText);
            var tekst = JSON.parse(responseText);
            currentFileId = tekst.id;
            var currentFolderId = document.getElementById('currentFolderId').value;
            var currentFolderName = document.getElementById('currentFolderName').innerHTML;
            getFilesInFolder(currentFolderId, currentFolderName, showFiles);
        },
        'text',
        'multipart/related; boundary=next_section',
        accessToken);
    
}

function saveText(text, fileId) {

    var url = 'https://www.googleapis.com/upload/drive/v3/files';
    url += '/' + fileId;

    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    var ajax = new Ajax();
    ajax.patchRequest(url,
        text,
       function (responseText) {
       },
        'text',
        'application/text',
        accessToken);
}

/**
 * Drive API Helper function uploadText
 * https://developers.google.com/drive/v3/web/manage-downloads
 * Simple upload: uploadType=media. 
 * For quick transfer of smaller files, for example, 5 MB or less.
 * 
 * @param id {string} the file id
 * @param name {string} the name of the file
 */
function downloadText(id, name) {
    currentFileId = id;
    currentFileName = name;
    var ajax = new Ajax();
    // Google CORS
    var url = 'https://www.googleapis.com/drive/v3/files';
    // id
    url += '/' + id;
    // download
    url += '?alt=media';
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    ajax.getRequest(url,
        function(responseText) {
            document.getElementById('editor').value = responseText;
        },
        'text',
        accessToken);
    
}

function newPage() {
    var textEditor = document.getElementById('editor');
    var vandaag = getFormattedDate();
    var editorInhoud =
        '<h1>Titel</h1>'
        + '\n' + '<div id=' + '\"' + 'chapeau' + '\"' + '>Chapeau</div>'
        + '\n<h2>Ondertitel</h2>'
        + '\n<h3>Onder ondertitel</h3>'
        + '\n<p>Paragraaf</p>'
        + '\n<div id=' + '\"' + 'author' + '\"' + '>JI</div>'
        + '\n<div id=' + '\"' + 'datetime' + '\"' + '>'
        + vandaag +'</div>'
        + '\n<div id=' + '\"' + 'keywords' + '\"' + '>&nbsp;</div>';

        textEditor.value = editorInhoud;

}

function getFormattedDate() {
    var weekdagen = ["Zondag", "Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag", "Zaterdag"];
    var maanden = ["Januari", "Februari", "Maart", "April", "Mei", "Juni","Juli", "Augustus", "September", "Oktober", "November", "December"];
    var today = new Date();
    var dd = today.getDate();
    if (dd < 10) {
        dd = '0' + dd
    }
    var mm = today.getMonth();
    var yyyy = today.getFullYear();
    var hr = today.getHours();
    var min = today.getMinutes();
    var sec = today.getSeconds();

    var datum = weekdagen[today.getDay()] + " " + dd + " " + maanden[today.getMonth()] + " " + yyyy + " " + hr + ":" + min + ":" +sec;
    return datum;
}

function autosave (){
    var text = document.getElementById('editor').value;
    if (text != "") {
        if (currentFileId === null) {
            prepareUploadText();

        }
        else {
            prepareSaveText();
        }
    }
  
}

