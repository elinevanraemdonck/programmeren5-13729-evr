/**
 * Load Drive API client library.
 * Samples: https://advancedweb.hu/2015/05/26/accessing-google-drive-in-javascript/
 */
function makeDriveApiCall() {
    gapi.client.load('drive', 'v3', getFiles);
}




/** 
 * One thing we should do is to first check if the myap folder is present, 
 * and only create it if it is not. 
 * This is a simple listing with filters to the directory mime type 
 * and not trashed (m.a.w. gedeleted door de gebruiker), 
 * then check if there is a result.
 */
function isMyapFolderPresent(responseText) {
    var response = JSON.parse(responseText);
    var alleFoldersEnFiles = response.files;

    /*var myapObject = alleFoldersEnFiles.filter(function(obj){
        return obj.name == "myap";}
        );*/
    if (alleFoldersEnFiles.some(function(item) {
        return item.name === 'myap';
    })){
    
     var myapObject = alleFoldersEnFiles.filter(function(obj){
         return obj.name === "myap";}
         );
    var myapId = myapObject[0].id;
    var myapName = myapObject[0].name;
 
    
        getFilesInFolder(myapId, myapName, showFiles);
    }
    else {
        createFolder('myap');
    }
}



/**
 * Drive API Helper function createFolder
 * Create a folder with the name given in the title parameter
 *
 * @param {string} title the name of the folder to be created
 */
function createFolder(name) {
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    var data = {
        name: name,
        mimeType: "application/vnd.google-apps.folder"
    }
    ajax.postRequest('https://www.googleapis.com/drive/v3/files',
        JSON.stringify(data),
        function(responseText) {
            alert(responseText);
        },
        'text',
        'application/json',
        accessToken);
}

/** 
 * We weten niet van te voren wat we met de bestanden gaan doen, 
 * vandaar dat je de callback methode als parameter kan meegeven.
 * Wordt er geen parameter meegegeven, gaan we ervan uit dat je
 * gewoon een lijst van de opgehaalde bestanden wilt tonen.
 * This is a simple listing with filters to the 
 * not trashed (m.a.w. gedeleted door de gebruiker).
 * 
 * @param {callbackFunction} the callback function for the ajax call
 */
var getFiles = function(callbackFunction = isMyapFolderPresent){

    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    // Google CORS
    var url = 'https://www.googleapis.com/drive/v3/files';

    // query 
    url += '?q=';
    url += 'not+trashed';
    // fields
    url += '&';
    url += 'fields=files(iconLink%2Cname%2CmimeType%2Cid%2Cparents)'; // %2C is html code for a comma (,)
    ajax.getRequest(url, callbackFunction, 'text', accessToken);
    // onthoud de id en de naam van de geselecteerde folder
    document.getElementById('currentFolderId').value = 'root';
    document.getElementById('currentFolderName').innerHTML = 'root';
}

/** 
 * Maak voor elk item in de lijst een li element met daarin de naam van het bestand.
 * Als het een folder is voegen we een knop toe waarop je kan klikken om de inhoud
 * van de folder te zien te krijgen
 * 
 * @param {responseText} het antwoord van de server in tekstformaat
 */
    var showFiles = function(responseText) {
        var ul = document.querySelector("#list");
        ul.innerHTML = '';
        //var li = document.createElement("li");
        //li.innerHTML = '<button type="button" onclick="getFiles();">Terug naar root</button>';
        //ul.appendChild(li);

        var response = JSON.parse(responseText);
        for (var i = 0; i < response.files.length; i++) {
            var alleFoldersEnFiles = response.files;
            var item = response.files[i];
            var li = document.createElement("li");
            var html = "<img src='" + item.iconLink + "'> ";
            if (item.mimeType == 'application/vnd.google-apps.folder') {
                html += '<button type="button" onclick="getFilesInFolder(\'' +
                    item.id + '\', \'' + item.name + '\');">' + item.name + '</button>';
            }
            else {
                html += item.name;
                // only html"
                if (item.mimeType === 'image/gif'|item.mimeType ==='image/jpeg'|'image/png'){
                    // only html"
                        html += ' <button type="button" onclick="getAllFiles(' + '\'' +
                           item.id + '\');">Link</button>';
                    }
                 else {
                    //if (item.name.indexOf('.html') > -1) 
                    if(item.mimeType === 'application/text'){
                            html += ' <button type="button" onclick="downloadText(' + '\'' +
                                item.id + '\', \'' + item.name + '\');">open</button>';
                            html += ' <button type="button" onclick="renameFile(' + '\'' +
                                item.id + '\', \'' + item.name + '\');">rename</button>';
                            html += ' <button type="button" onclick="getAllFiles(' + '\'' +
                               item.id + '\');">Link</button>';

                        }
                    }
            }
            html += ' <button type="button" onclick="deleteFile(' + '\'' +
                item.id + '\', \'' + item.name + '\');">delete</button>';
            li.innerHTML = html;
            ul.appendChild(li);
        }
    }
            

/** 
 * We weten niet van te voren wat we met de bestanden gaan doen, 
 * vandaar dat je de callback methode als parameter kan meegeven.
 * Wordt er geen parameters meegegeven, gaan we ervan uit dat je
 * gewoon een lijst van de opgehaalde bestanden wilt tonen.
 * This is a simple listing with filters to the parent directory 
 * and not trashed (m.a.w. gedeleted door de gebruiker).
 * 
 * @param {id} de id van de te tonen folder
 * @param {name} de naam van de te tonen folder
 * @param {callbackFunction} the callback function for the ajax call
 */
function getFilesInFolder(id, name, callbackFunction = showFiles) {
    var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
    // or this: gapi.auth.getToken().access_token;
    var ajax = new Ajax();
    // google CORS
    var url = 'https://www.googleapis.com/drive/v3/files';
    // query 
    url += '?q=';
    url += 'not+trashed';
    url += '+and+';
    url += '\'' + id + '\'+in+parents';
    // fields
    url += '&';
    url += 'fields=files(iconLink%2Cname%2CmimeType%2Cid%2Cparents)';
    //alert(url);
    ajax.getRequest(url, callbackFunction, 'text', accessToken);
    // onthoud de id en de naam van de geselecteerde folder
    document.getElementById('currentFolderId').value = id;
    //document.getElementById('currentFolderName').value = name;
    //document.getElementById('currentFolderName').childNodes[0].nodeValue = name;
    document.getElementById('currentFolderName').innerHTML = name;
}

    /**
     * Prepare the creation of a new folder
     * Validate the foldername
     * Check if new foldername does already exist
     */
    function prepareCreateFolder() {
        var folderName = document.getElementById('folderName').value;
        // haal de Id van de parent folder op
        var parentFolderId = document.getElementById('currentFolderId').value;
        var parentFolderName = document.getElementById('currentFolderName').value;

        if (folderName.length > 0) {
            var regExpresion = new RegExp("[^a-zA-Z0-9_. ]+");
            if (regExpresion.test(folderName)) {
                alert('Ongeldige foldernaam: ' + folderName)
            }
            else {
                // haal alle bestanden van de google drive op en verifiëer
                // als de folder al bestaat, daarvoor geven we de callback
                // functie doesFolderExist mee
                if (parentFolderId == 'root') {
                    getFiles(function(responseText) {
                        doesFolderExist(responseText,
                            folderName, parentFolderId);
                    });
                }
                else {
                    // als de gebruiker een map geselecteerd heeft halen
                    // we alleen de bestanden in de geselecteerde map op
                    getFilesInFolder(parentFolderId,
                        parentFolderName,
                        function(responseText) {
                            doesFolderExist(responseText,
                                folderName, parentFolderId);
                        });
                }
            }
        }
        else {
            alert('Typ eerst een naam voor de folder in.');
        }
    }

    /** 
     * Ga na als de folder al bestaat of niet.
     * Als de folder al bestaat stuur een boodschap dat de folder bestaat.
     * Als de folder in de root staat roepen we CreatFolder op
     * anders CreateFolderInParent
     * 
     * @param {responseText} het antwoord van de server in tekstformaat
     * @param {folderName} de naam van de te maken folder
     * @param {parentFolderId} de id folder waarin de nieuwe map gemaakt moet worden
     */
    var doesFolderExist = function(responseText, folderName, parentFolderId) {
        var response = JSON.parse(responseText);
        // check is the folder already exists
        // names not case sensitive
        if (response.files.some(function(item) {
                return item.name.toLowerCase() === folderName.toLowerCase()
        })) {
            alert('Folder met de naam ' + folderName + ' bestaat al!')
        }
        else {
            if (folderName == 'root') {
                createFolder(folderName);
            }
            else {
                createFolderInParent(folderName, parentFolderId);
            }
        }
    }

    /**
     * Drive API Helper function createFolderInParent
     * Create a folder with the name given in the title parameter
     * in the root folder (myap)
     *
     * @param {string} title the name of the folder to be created
     * @param {parentId} the id of the parent folder
     */
    function createFolderInParent(name, parentId) {
        var data = {
            'name': name,
            'mimeType': "application/vnd.google-apps.folder",
            'parents': [parentId]
        };

        var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
        // or this: gapi.auth.getToken().access_token;
        var ajax = new Ajax();
        ajax.postRequest('https://www.googleapis.com/drive/v3/files',
            JSON.stringify(data),
            function(responseText) {
                alert("New folder created:" + responseText);
            },
            'text',
            'application/json',
            accessToken);
    }

    /**
     * Drive API Helper function deleteFile
     * Delete a folder or a file based in it's Id
     * Permanently deletes a file owned by the user without moving it to the trash. 
     * If the target is a folder, all descendants owned by the user are also deleted
     * https://developers.google.com/drive/v3/reference/files/delete
     * 
     * @param id {string} the id of the item to be deleted
     * @param name {string} the name of the item to be deleted
     */
    function deleteFile(id, name) {
        var ajax = new Ajax();
        // Google CORS
        var url = 'https://www.googleapis.com/drive/v3/files';
        // id
        url += '/' + id;
        var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
        // or this: gapi.auth.getToken().access_token;
        ajax.deleteRequest(url,
            function(responseText) {
                alert(name + ' is gedeleted!');
                var currentFolderId = document.getElementById('currentFolderId').value;
                var currentFolderName = document.getElementById('currentFolderName').innerHTML;
                getFilesInFolder(currentFolderId,currentFolderName,showFiles);
            },
            accessToken);

    }

    function renameFile(id, newName){
        var newFileName = document.getElementById("fileName").value;

        if (newFileName.length > 0) {
            var regExpresion = new RegExp("[^a-zA-Z0-9_. ]+");
            if (regExpresion.test(newFileName)) {
                alert('Ongeldige foldernaam: ' + newFileName)
            }
            else{
                var data = {
                    'name': newFileName +'.html',
                    'fileId': id
                };
                var url = 'https://www.googleapis.com/drive/v3/files';
                // id
                url += '/' + id;

                var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
                // or this: gapi.auth.getToken().access_token;
                var ajax = new Ajax();
                ajax.patchRequest(url,
                    JSON.stringify(data),
                    function(responseText) {
                        alert("Filename changed" + responseText);
                        var currentFolderId = document.getElementById('currentFolderId').value;
                        var currentFolderName = document.getElementById('currentFolderName').innerHTML;
                        getFilesInFolder(currentFolderId,currentFolderName,showFiles);
                    },
                    'text',
                    'application/json',
                    accessToken);
            }
        }
        else {
            alert('Geef eerst een nieuwe naam voor het bestand in.');
        }

    }

    var fileId;

    var getAllFiles = function(id, callbackFunction = linkFile) {
        fileId = id;
        var accessToken = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().access_token;
        // or this: gapi.auth.getToken().access_token;
        var ajax = new Ajax();
        // Google CORS
        var url = 'https://www.googleapis.com/drive/v3/files';
        // query 
        url += '?q=';
        url += 'not+trashed';
        // fields
        url += '&';
        url += 'fields=files(iconLink%2Cname%2CmimeType%2Cid%2Cparents)'; // %2C is html code for a comma (,)
        ajax.getRequest(url, callbackFunction, 'text', accessToken);
    }

        function linkFile(responseText) {
            var id = fileId;
            var response = JSON.parse(responseText);
            var alleFoldersEnFiles = response.files;
            var link;
    
            var currentFile = alleFoldersEnFiles.filter(function(obj){
                return obj.id == id;}
            );
            var currentFileMimeType = currentFile[0].mimeType;
            var currentFileName = currentFile[0].name;
            var parentFolderId = currentFile[0].parents;
            link = currentFileName;
    
            do
            {
                var parentFolderObject = alleFoldersEnFiles.filter(function(obj){
                    return obj.id == parentFolderId;}
               );
        
                if (parentFolderObject[0])
                {
                    parentFolderId = parentFolderObject[0].parents;
                    var parrentFolderName = parentFolderObject[0].name;
        
                    link = parrentFolderName + '/' + link;
                }
        
            }
            while (parentFolderObject[0])
    
            insertHtmlInTextarea(link, currentFileName, currentFileMimeType);
    
        }


        function insertHtmlInTextarea(link, currentFileName, currentFileMimeType) {
            if (currentFileMimeType === "image/gif"| currentFileMimeType === "image/png"| currentFileMimeType === "image/jpeg")
            {
                var fileName = currentFileName.replace(".gif","").replace(".png","").replace(".jpeg","").replace(".JPG","");
                var newText = '<figure><img alt=' + '\"' + fileName + '\"' + 'src=' + '\"' + link
                + '\"/><figcaption>' + fileName + '</figcaption></figure>';
            }
            else
            {
                var fileName = currentFileName.replace(".html","");
                var newText = '<a href=' + '\"' + link + '\"' + '>' +fileName + '</a>';
            }
            var el = document.getElementById('editor');
            var start = el.selectionStart
            var end = el.selectionEnd
            var text = el.value
            var before = text.substring(0, start)
            var after = text.substring(end, text.length)
            el.value = (before + newText + after)
            el.selectionStart = el.selectionEnd = start + newText.length
            el.focus()
        }

       